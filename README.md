# CRA Exemplo com NX React Components

Repositório com exemplos de aplicação dos componentes do NX Design System - React Components 

 - Exemplo de botões;
 - Exemplo de Ícones;
 - Exemplo de tipografia.

## Instalação

Link do repositório: [https://www.npmjs.com/package/@linx-digital/nx-react-components]
Para rodar o projeto você precisa instalar as dependências com o comando:

### `yarn`

E para rodar o projeto:
### `yarn start`

Roda o projeto em modo de desenvolvimento.\
Acesse [http://localhost:3000](http://localhost:3000) para visualizar no browser.

A página irá recarregar se você editar.\
Você também verá qualquer erro de lint no console.

