import './App.css';
import Buttons from './Components/Buttons';
import Typography from './Components/Typographies';

function App() {
  return (
    <div className="App">
      <h1>NX Design System - React Components</h1>
      <Buttons />
      <Typography />
    </div>
  );
}

export default App;
