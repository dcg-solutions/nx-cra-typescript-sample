import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: fit-content;
  display: flex;
  flex-wrap: wrap;
  align-items: center; 
  justify-content: center;
  flex-direction: row;
  gap: 0.8rem;
`;

export const ContainerWrapper = styled.div`
  width: 100%;
  height: 200px;
  margin-bottom: '12px';
`;
