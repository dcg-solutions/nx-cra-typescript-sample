import React from 'react';
import { Button } from '@linx-digital/nx-react-components';
import { IconLove } from '@linx-digital/nx-react-components/Icons';
import { ContainerWrapper, Container } from '../Commons/Container';

const Buttons: React.FC = () => {
  return (
    <ContainerWrapper>
      <h2> Exemplo de implementação dos Botões</h2>
      <Container>
        <Button borderRadius="small" variant="primary">
          Normal
        </Button>
        <Button borderRadius="small" variant="primary" iconLeft={<IconLove />}>
          Normal
        </Button>
        <Button borderRadius="small" variant="primary" iconRight={<IconLove />}>
          Normal
        </Button>
        <Button
          borderRadius="small"
          variant="primary"
          iconLeft={<IconLove />}
        />
        <Button borderRadius="large" variant="primary">
          Normal
        </Button>
        <Button borderRadius="large" variant="primary" iconLeft={<IconLove />}>
          Normal
        </Button>
        <Button borderRadius="large" variant="primary" iconRight={<IconLove />}>
          Normal
        </Button>
        <Button
          borderRadius="large"
          variant="primary"
          iconLeft={<IconLove />}
        />
      </Container>
    </ContainerWrapper>
  );
};

export default Buttons;
