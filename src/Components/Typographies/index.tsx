import React from 'react';
import { Typography } from '@linx-digital/nx-react-components';
import { ContainerWrapper } from '../Commons/Container';

const Typographies: React.FC = () => {
  return (
    <ContainerWrapper>
      <h2> Exemplo de implementação da Tipografia</h2>
      <Typography
        element="h1"
        fontWeight="font-font-weight-font-weight-light"
        fontFamily="font-family-roboto"
      >
        The quick brown fox jumps over the lazy dog
      </Typography>
      <Typography element="p">
        The quick brown fox jumps over the lazy dog
      </Typography>
    </ContainerWrapper>
  );
};

export default Typographies;
